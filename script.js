/* 
Теоретичні питання

1. Опишіть своїми словами що таке Document Object Model (DOM)

Це програмний інтерфейс для веб-документів. Він являє собою сторінку, за допомогою якої програми можуть змінювати структуру, стиль і зміст документа. DOM представляє документ у вигляді вузлів та об'єктів; Таким чином, мови програмування можуть взаємодіяти зі сторінкою.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

innerHTML - властивість повертає текст, включаючи всі пробіли та внутрішні теги елемента. Вона зберігає форматування тексту та всі додаткові теги, такі як <b>, <i> тощо.
innerText - властивість повертає лише текст, видаляючи пробіли та внутрішні теги.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
 - getElement - ByClassName/ByTagName/ById/..
 - querySelector -  all/#/./..

Практичні завдання
 1. 
- Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
- Використайте 2 способи для пошуку елементів.
- Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).

//  2. Змініть текст усіх елементів h2 на "Awesome feature".

//  3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
*/

// Практичні завдання №1
const featureElements = document.getElementsByClassName("feature");

for (let i = 0; i < featureElements.length; i++) {
  featureElements[i].style.textAlign = "center";
  console.log(featureElements[i]);
}

const firstWay = document.querySelectorAll(".feature-description");
console.log(firstWay);
const secondWay = document.getElementsByClassName("feature-description");
console.log(secondWay);

// Практичні завдання №2

const headers = document.querySelectorAll("h2");

headers.forEach((header) => {
  header.textContent = "Awesome feature";
});

// Практичні завдання №3

const featureTitles = document.querySelectorAll(".feature-title");

featureTitles.forEach((title) => {
  title.textContent += "!";
});
